import webSocket from '@ohos.net.webSocket';
import util from '@ohos.util';
import cryptoFramework from '@ohos.security.cryptoFramework';
import buffer from '@ohos.buffer';
import Url from "@ohos.url";

const base64 = new util.Base64Helper();
const textEncoder = new util.TextEncoder();

export namespace tts {
    export interface TTSAccount {
        appId: string;
        accessKeyId: string;
        accessKetSecret: string;
    }

    export interface AudioParams {
        sample_rate?: string;
        audio_code?: string;
        frame_size?: number;
        speaker: string;
        speed?: number;
        volume?: number;
        pitch?: number;
        py?: number;
    }

    export interface TextParams {
        text: string;
        status: number;
        id: number;
        reset_params?: AudioParams;
    }

    export class TTS {
        private url: string = 'wss://ai-test-integ-env.iflyrec.com/tts';
        private account: TTSAccount;
        private audioParams: AudioParams;
        private ws: webSocket.WebSocket;
        private connectStatus: boolean = false;
        private fullJsonStr: string = '';
        private audioResult: Uint8Array;
        private successCallback: (data: Uint8Array) => void;
        private connectCallback: () => void;

        constructor(account: TTSAccount, audioParams: AudioParams, serviceUrl?: string) {
            this.account = account;
            this.audioParams = audioParams;
            this.url = serviceUrl ?? this.url;
            this.ws = webSocket.createWebSocket();
            this.initListener();
        }

        initListener() {
            this.ws.on('open', (err, value) => {
                this.connectStatus = true
            })
            this.ws.on('message', (err, value) => {
                this.connectStatus = true;
                this.spliceJsonStr(value);
            })
            this.ws.on('close', (err, value) => {
                this.connectStatus = false;
            })
            this.ws.on('error', (err) => {
                this.connectStatus = false;
            })
        }

        spliceJsonStr(str) {
            this.fullJsonStr += str;
            try {
                const jsonObj = JSON.parse(this.fullJsonStr);
                this.fullJsonStr = '';
                if (jsonObj?.data?.action === 'started') {
                    this.connectCallback();
                }
                if (jsonObj?.data?.audio) {
                    let result: Uint8Array = base64.decodeSync(jsonObj?.data?.audio);
                    const buf = buffer.concat([this.audioResult, result]);
                    this.audioResult = new Uint8Array(buf.buffer);
                    if (jsonObj?.data?.status === 2) {
                        this.successCallback?.(this.audioResult)
                    }
                }
            } catch (err) {
                console.log(JSON.stringify(err));

            }
        }

        async connect(connectCallback: () => void): Promise<void> {
            if (this.connectStatus) {
                return
            }
            this.connectCallback = connectCallback;
            const generalParams: string = await this.getGeneralParams();
            let objectParams = new Url.URLParams();
            for (const key in this.audioParams) {
                objectParams.append(key, this.audioParams[key])
            }
            const url = `${this.url}?${generalParams}&${objectParams.toString()}`;
            this.ws.connect(url);
        }

        async sendMessage(params: TextParams, success: (data: Uint8Array) => void) {
            try {
                this.successCallback = success;
                this.audioResult = new Uint8Array();
                const data = JSON.stringify({
                    ...params,
                    text: base64.encodeToStringSync(textEncoder.encodeInto(params.text))
                })
                await this.ws.send(data);
            } catch(err) {
                console.log(JSON.stringify(err))
            }
        }

        async getGeneralParams(): Promise<string> {
            try {
                const now = new Date();
                const utc = `${now.getFullYear()}-${(now.getMonth() + 1).toString().padStart(2, '0')}-${now.getDate().toString().padStart(2, '0')}`
                + `T${now.getHours().toString().padStart(2, '0')}:${now.getMinutes().toString().padStart(2, '0')}:${now.getSeconds().toString().padStart(2, '0')}+0800`;
                const uuid = util.randomUUID();
                const baseString = `accessKeyId=${encodeURIComponent(this.account.accessKeyId)}&appId=${encodeURIComponent(this.account.appId)}&utc=${encodeURIComponent(utc)}&uuid=${encodeURIComponent(uuid)}`;
                const mac = cryptoFramework.createMac('SHA1');
                const KeyBlob = {
                    data: textEncoder.encodeInto(this.account.accessKetSecret)
                };
                const symKeyGenerator = cryptoFramework.createSymKeyGenerator('AES256');
                const symKey = await symKeyGenerator.convertKey(KeyBlob);
                await mac.init(symKey);
                await mac.update({ data: textEncoder.encodeInto(baseString) });
                const result = await mac.doFinal();
                const sign = base64.encodeToStringSync(result.data);
                return `${baseString}&signature=${encodeURIComponent(sign)}`;
            } catch(err) {
                console.log(JSON.stringify(err));
            }
        }
    }
}