import webSocket from '@ohos.net.webSocket';
import util from '@ohos.util';
import cryptoFramework from '@ohos.security.cryptoFramework';
import buffer from '@ohos.buffer';
import Url from "@ohos.url";

const base64 = new util.Base64Helper();
const textEncoder = new util.TextEncoder();

export namespace asr {
    export interface Account {
        appId: string;
        accessKeyId: string;
        accessKetSecret: string;
    }

    export interface BasicParams {
        lang: string;
        audioEncode: string;
        samplerate: number;
        sn?: string;
        sourceInfo?: number;
        punc?: number;
    }

    export interface RoleParams {
        contextId?: string;
        roleType?: number;
    }

    export interface OptimizationParams {
        eng_seg_max?: number;
        eng_seg_min?: number;
        eng_seg_weight?: number;
        eng_pgsnum?: number;
        eng_vad_mdn?: number;
        eng_language_type?: number;
        hotWorldId?: number;
        eng_dyhotws?: number;
        eng_rlang?: number;
        pd?: number;
    }

    export interface TranslateParams {
        target_lang?: string;
        trans_strategy?: number;
    }

    export interface TextParams {
        text: string;
        status: number;
        id: number;
        reset_params?: BasicParams
    }

    export interface QueryPrams extends BasicParams, RoleParams, OptimizationParams, TranslateParams {}

    export class ASR {
        private url: string = 'wss://api.iflyrec.com/ast';
        private account: Account;
        private queryParams: QueryPrams;
        private ws: webSocket.WebSocket;
        private connectStatus: boolean = false;
        private fullJsonStr: string = '';
        private audioResult: Uint8Array;
        private successCallback: (data: string) => void;
        private connectCallback: () => void;
        private transResult: string = '';
        private maxBuf: number = 1280;

        constructor(account: Account, queryParams: QueryPrams, serviceUrl?: string) {
            this.account = account;
            this.queryParams = queryParams;
            this.url = serviceUrl ?? this.url;
            this.ws = webSocket.createWebSocket();
            this.initListener();
        }

        initListener() {
            this.ws.on('open', (err, value) => {
                this.connectStatus = true
            })
            this.ws.on('message', (err, value) => {
                this.connectStatus = true;
                this.spliceJsonStr(value);
            })
            this.ws.on('close', (err, value) => {
                this.connectStatus = false;
            })
            this.ws.on('error', (err) => {
                this.connectStatus = false;
            })
        }

        spliceJsonStr(str) {
            this.fullJsonStr += str;
            try {
                const jsonObj = JSON.parse(this.fullJsonStr);
                this.fullJsonStr = '';
                if (jsonObj?.action === 'started') {
                    this.connectCallback();
                } else if (jsonObj?.cn?.st) {
                    if (jsonObj.cn.st.type == 0) {
                        let result: any[] = jsonObj.cn.st.rt[0].ws;
                        result.forEach(item => {
                            this.transResult += item?.cw[0].w;
                        })
                    }
                }
                if (jsonObj?.ls) {
                    this.successCallback(this.transResult);
                }
            } catch (err) {
                console.log(JSON.stringify(err));

            }
        }

        async connect(connectCallback: () => void): Promise<void> {
            try {
                if (this.connectStatus) {
                    return
                }
                this.connectCallback = connectCallback;
                const generalParams: string = await this.getGeneralParams();
                let objectParams = new Url.URLParams();
                for (const key in this.queryParams) {
                    objectParams.append(key, this.queryParams[key] + '')
                }
                const url = `${this.url}?authString=${generalParams}&${objectParams.toString()}`;
                this.ws.connect(url);
            } catch (err) {
                console.log(err)
            }
        }

        async sendMsg(buf: ArrayBuffer | string, success?: () => void) {
            try {
                await this.ws.send(buf);
            } catch (error) {
                console.log(JSON.stringify(error));
            }
        }

        sendMessage(buf: ArrayBuffer, success: (data: string) => void) {
            try {
                this.successCallback = success;
                const count = Math.ceil(buf.byteLength / this.maxBuf);
                for (let i = 0; i <= count; i++) {
                    ((index) => {
                        setTimeout(() => {
                            if (index === count) {
                                this.sendMsg(JSON.stringify({
                                    end: true
                                }))
                            } else {
                                let begin = index * this.maxBuf;
                                let end = (begin + 1280 > buf.byteLength) ? buf.byteLength : (begin + 1280);
                                let newBuf = buf.slice(begin, end);
                                this.sendMsg(newBuf);
                            }
                        }, index * 40);
                    })(i)
                }
            } catch (err) {
                console.log(JSON.stringify(err))
            }
        }

        async getGeneralParams(): Promise<string> {
            try {
                const versionNumber = 'v1.0';
                const now = new Date();
                const utc = `${now.getFullYear()}-${(now.getMonth() + 1).toString().padStart(2, '0')}-${now.getDate().toString().padStart(2, '0')}`
                + `T${now.getHours().toString().padStart(2, '0')}:${now.getMinutes().toString().padStart(2, '0')}:${now.getSeconds().toString().padStart(2, '0')}+0800`;
                const uuid = util.randomUUID();
                const baseString = encodeURIComponent(`${versionNumber},${this.account.appId},${this.account.accessKeyId},${utc},${uuid}`);
                const mac = cryptoFramework.createMac('SHA1');
                const KeyBlob = {
                    data: textEncoder.encodeInto(this.account.accessKetSecret)
                };
                const symKeyGenerator = cryptoFramework.createSymKeyGenerator('AES256');
                const symKey = await symKeyGenerator.convertKey(KeyBlob);
                await mac.init(symKey);
                await mac.update({ data: textEncoder.encodeInto(baseString) });
                const result = await mac.doFinal();
                const sign = base64.encodeToStringSync(result.data);
                return `${baseString}${encodeURIComponent(',' + sign)}`;
            } catch (err) {
                console.log(JSON.stringify(err));
            }
        }
    }
}