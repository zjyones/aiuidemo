import webSocket from '@ohos.net.webSocket';
import util from '@ohos.util';
import cryptoFramework from '@ohos.security.cryptoFramework';
import buffer from '@ohos.buffer';
import Url from "@ohos.url";

const base64 = new util.Base64Helper();
const textEncoder = new util.TextEncoder();

export namespace aiui {
    export interface Account {
        appId: string;
        appKey: string;
        signtype?: string;
    }

    export interface GeneralParams {
        scene: string;
        auth_id?: string;
        data_type: string;
        interact_mode?: string;
        close_delay?: number;
    }

    export interface SemanticParams {
        lat?: string;
        lng?: number;
        topn?: number;
        per_param?: string;
        clean_dialog_history?: string;
    }

    export interface IdentifiesParams {
        aue?: string;
        sample_rate?: string;
        sppx_size?: number;
        result_level?: string;
        val_info?: string;
        cloud_vad_eos?: string;
    }

    export interface BusinessParams extends GeneralParams, SemanticParams, IdentifiesParams {}

    export class AIUI {
        private url: string = 'wss://wsapi.xfyun.cn/v1/aiui';
        private account: Account;
        private businessParams: BusinessParams;
        private ws: webSocket.WebSocket;
        private connectStatus: boolean = false;
        private fullJsonStr: string = '';
        private audioResult: Uint8Array;
        private successCallback: (data: string) => void;
        private connectCallback: () => void;
        private maxBuf: number = 1280;

        constructor(account: Account, businessParams: BusinessParams, serviceUrl?: string) {
            this.account = account;
            this.businessParams = businessParams;
            this.url = serviceUrl ?? this.url;
            this.ws = webSocket.createWebSocket();
            this.initListener();
        }

        initListener() {
            this.ws.on('open', (err, value) => {
                this.connectStatus = true
            })
            this.ws.on('message', (err, value) => {
                this.connectStatus = true;
                this.spliceJsonStr(value);
            })
            this.ws.on('close', (err, value) => {
                this.connectStatus = false;
            })
            this.ws.on('error', (err) => {
                this.connectStatus = false;
            })
        }

        spliceJsonStr(str) {
            this.fullJsonStr += str;
            try {
                const jsonObj = JSON.parse(this.fullJsonStr);
                this.fullJsonStr = '';
                if (jsonObj?.action === 'started') {
                    this.connectCallback();
                }
                if (jsonObj?.data?.audio) {
                    let result: Uint8Array = base64.decodeSync(jsonObj?.data?.audio);
                    const buf = buffer.concat([this.audioResult, result]);
                    this.audioResult = new Uint8Array(buf.buffer);
                    if (jsonObj?.data?.status === 2) {
                        //                        this.successCallback?.(this.audioResult)
                    }
                }
            } catch (err) {
                console.log(JSON.stringify(err));

            }
        }

        async connect(connectCallback: () => void): Promise<void> {
            if (this.connectStatus) {
                return
            }
            this.connectCallback = connectCallback;
            const generalParams: string = await this.getGeneralParams();
            const url = `${this.url}?${generalParams}`;
            this.ws.connect(url);
        }

        async sendMsg(buf: ArrayBuffer | string, success?: () => void) {
            try {
                await this.ws.send(buf);
            } catch (error) {
                console.log(JSON.stringify(error));
            }
        }

        sendMessage(buf: ArrayBuffer, success: (data: string) => void) {
            try {
                this.successCallback = success;
                const count = Math.ceil(buf.byteLength / this.maxBuf);
                for (let i = 0; i <= count; i++) {
                    ((index) => {
                        setTimeout(() => {
                            if (index === count) {
                                this.sendMsg('--end--')
                            } else {
                                let begin = index * this.maxBuf;
                                let end = (begin + 1280 > buf.byteLength) ? buf.byteLength : (begin + 1280);
                                let newBuf = buf.slice(begin, end);
                                this.sendMsg(newBuf);
                            }
                        }, index * 40);
                    })(i)
                }
            } catch (err) {
                console.log(JSON.stringify(err))
            }
        }

        async getGeneralParams(): Promise<string> {
            try {
                const paramBase64 = base64.encodeToStringSync(textEncoder.encodeInto(JSON.stringify(this.businessParams)))
                const curtime = Math.floor(Date.now() / 1000);
                const originStr = this.account.appKey + curtime + paramBase64;
                const md = cryptoFramework.createMd(this.account.signtype ?? 'MD5');
                await md.update({ data: textEncoder.encodeInto(originStr) });
                const result = await md.digest();
                const checksum = arr2hex(result.data);
                return `appid=${this.account.appId}&checksum=${checksum}&curtime=${curtime}&param=${paramBase64}`;
            } catch (err) {
                console.log(JSON.stringify(err));
            }
        }
    }
}

function arr2hex(arr) {
    return Array.prototype.map.call(arr, x => ('00' + x.toString(16))).slice(2).join('');
}